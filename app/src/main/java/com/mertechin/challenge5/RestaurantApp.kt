package com.mertechin.challenge5

import android.app.Application
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin

class RestaurantApp : Application() {
	override fun onCreate() {
		super.onCreate()
		initKoin()
	}

	private fun initKoin() {
		startKoin {
			androidLogger()
			androidContext(this@RestaurantApp)
			modules(AppModules.modules)
		}
	}
}
