package com.mertechin.challenge5.ui.checkout

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import com.mertechin.challenge5.adapter.CheckoutAdapter
import com.mertechin.challenge5.api.ApiClient
import com.mertechin.challenge5.api.ApiServices
import com.mertechin.challenge5.databinding.ActivityCheckoutBinding
import com.mertechin.challenge5.dataclass.api_dataclass.OrderData
import com.mertechin.challenge5.dataclass.api_dataclass.OrderMenu
import com.mertechin.challenge5.dataclass.api_dataclass.OrderRes
import com.mertechin.challenge5.ui.success.SuccessActivity
import kotlinx.coroutines.launch
import org.koin.androidx.viewmodel.ext.android.viewModel
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class CheckoutActivity : AppCompatActivity() {
	private val apiServices: ApiServices = ApiClient.ApiClient.instance
	private val checkOutViewModel: CheckoutViewModel by viewModel()
	private lateinit var binding: ActivityCheckoutBinding
	private lateinit var checkoutAdapter: CheckoutAdapter
	private lateinit var listOrderMenu: List<OrderMenu>
	private lateinit var listOrderData: List<OrderData>

	override fun onCreate(savedInstanceState: Bundle?) {
		super.onCreate(savedInstanceState)

		// Inisialisasi properti
//		listOrderMenu = emptyList()
		listOrderData = emptyList()

		binding = ActivityCheckoutBinding.inflate(layoutInflater)
		setContentView(binding.root)

		val rvCheckout = binding.rcvCheckout
		rvCheckout.layoutManager = LinearLayoutManager(this)

		// Setup ViewModel
//		val checkOutViewModel = ViewModelProvider(this).get(CheckoutViewModel::class.java)
		checkOutViewModel.getTotalHarga(this,this)
		checkOutViewModel.initSharedPref(this)
		checkOutViewModel.getCart(this).observe(this) { data ->
			// Inisialisasi adapter dan set adapter setelah data tersedia
			checkoutAdapter = CheckoutAdapter(data)
			rvCheckout.adapter = checkoutAdapter

			// Mapping Isi data request POST Method
			listOrderData = data.map { orderData ->
				OrderData(
					nama    = orderData.name,
					qty     = orderData.quantity.toByte(),
					catatan = orderData.note,
					harga   = orderData.price
				)
			}
		}

		checkOutViewModel.getSUM(this).observe(this) {
			binding.tvTotalCheckout.text = it.toString()
		}

		binding.btnPesan.setOnClickListener {
			// Panggil API POST Method
			val user = checkOutViewModel.getSharedPref().toString()
			val total = checkOutViewModel.totalHarga

				Log.d("Sending Order","Sending order: $user, $total, $listOrderData")

				lifecycleScope.launch {
					try {
						val res = apiServices.sendOrder(OrderMenu(user, total, listOrderData))
						// Handle the response here

						res.enqueue(object : Callback<OrderRes> {
							override fun onResponse(
								call: Call<OrderRes>, response: Response<OrderRes>
							) {
								val intent = Intent(this@CheckoutActivity,SuccessActivity::class.java)
						        startActivity(intent)
								checkOutViewModel.deleteData()
							}

							override fun onFailure(call: Call<OrderRes>, t: Throwable) {
								Toast.makeText(this@CheckoutActivity, t.toString(), Toast.LENGTH_SHORT).show()
							}
						})
					} catch (e: Exception) {
						e.printStackTrace()
					}
				}
		}
	}
}