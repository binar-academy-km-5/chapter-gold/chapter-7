package com.mertechin.challenge5.ui.detail

import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.mertechin.challenge5.dataclass.api_dataclass.ListData
import com.mertechin.challenge5.dataclass.room_dataclass.Restoran
import com.mertechin.challenge5.repository.RestaurantRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class DetailViewModel(private val repo: RestaurantRepository) : ViewModel() {

	private val _data = MutableLiveData<ListData>()
	val data: LiveData<ListData> get() = _data

	fun setData(data: ListData) {
		_data.value = data
	}
	fun insertItem(context: Context, restoranData: Restoran){
		viewModelScope.launch(Dispatchers.IO){
			repo.insertData(restoranData)
		}
	}

	fun updateItem(context: Context, restoranData: Restoran){
		val tempData= repo.findSomeDataByName(restoranData.name)

		viewModelScope.launch(Dispatchers.IO){
			repo.updateQuantity(tempData.id,tempData.quantity+1)
		}
	}

	fun findItem(context: Context, restoranData: Restoran): Boolean {
		val cartItems = repo.findItemCart(restoranData.name)
		return cartItems?.isNotEmpty() == true
	}
}