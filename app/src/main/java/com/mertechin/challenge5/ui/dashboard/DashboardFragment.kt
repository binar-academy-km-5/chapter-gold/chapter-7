package com.mertechin.challenge5.ui.dashboard

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.mertechin.challenge5.R
import com.mertechin.challenge5.adapter.MenuAdapter
import com.mertechin.challenge5.databinding.FragmentDashboardBinding
import com.mertechin.challenge5.dataclass.api_dataclass.ListData
import com.mertechin.challenge5.ui.detail.DetailActivity
import org.koin.androidx.viewmodel.ext.android.viewModel

class DashboardFragment : Fragment() {
	private var isGrid = true
//	private lateinit var vmDashboard: DashboardViewModel
	private val vmDashboard: DashboardViewModel by viewModel()
	private lateinit var menuAdapter: MenuAdapter
	private var _binding: FragmentDashboardBinding? = null

	// This property is only valid between onCreateView and
	// onDestroyView.
	private val binding get() = _binding!!

	override fun onCreateView(
		inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
	): View {
//		vmDashboard = ViewModelProvider(this).get(DashboardViewModel::class.java)
		vmDashboard.initSharedPref(requireContext())

		isGrid = vmDashboard.getSharedPref(true)

		vmDashboard.getMenuData()
		vmDashboard.menu.observe(viewLifecycleOwner, Observer { menu ->
			menuAdapter = MenuAdapter(isGrid, menu)
			setupRecyclerView(isGrid)
		})

		_binding = FragmentDashboardBinding.inflate(inflater, container, false)

		// Set up RecyclerView
		setupActionChangeLayout()

		val root: View = binding.root
		return root
	}

	override fun onDestroyView() {
		super.onDestroyView()
		_binding = null
	}

	 fun setupRecyclerView(isGrid: Boolean) {
		val rvData = binding.rvData
		val change = binding.gantiLayout

		menuAdapter.setOnItemClickCallback(object : MenuAdapter.OnItemClickCallback {
			override fun onItemClicked(data: ListData) {
				val intentDetail = Intent(requireContext(), DetailActivity::class.java)
				intentDetail.putExtra("DATA", data)
				startActivity(intentDetail)
			}
		})

		rvData.adapter = menuAdapter

		if (isGrid) {
			rvData.layoutManager = GridLayoutManager(requireContext(), 2)
			change.setImageResource(R.drawable.ic_baseline_apps_24)
		} else {
			rvData.layoutManager = LinearLayoutManager(requireContext())
			change.setImageResource(R.drawable.ic_baseline_format_list_bulleted_24)
		}
	}

	 fun setupActionChangeLayout() {
		val change = binding.gantiLayout
		change.setOnClickListener {
			isGrid = !isGrid
			setupRecyclerView(isGrid)

			// Merubah nilai sharedPref
			Log.w("Nilai Grid",isGrid.toString())
			vmDashboard.changeValue(isGrid)
		}
	}
}