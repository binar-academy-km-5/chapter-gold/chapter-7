package com.mertechin.challenge5.ui.detail

import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import com.bumptech.glide.Glide
import com.mertechin.challenge5.MainActivity
import com.mertechin.challenge5.databinding.ActivityDetailBinding
import com.mertechin.challenge5.dataclass.api_dataclass.ListData
import com.mertechin.challenge5.dataclass.room_dataclass.Restoran
import org.koin.androidx.viewmodel.ext.android.viewModel

class DetailActivity : AppCompatActivity() {
	private lateinit var binding : ActivityDetailBinding
	private val vmDetail: DetailViewModel by viewModel()
//	private lateinit var vmDetail: DetailViewModel

	override fun onCreate(savedInstanceState: Bundle?) {
		super.onCreate(savedInstanceState)

//		vmDetail   = ViewModelProvider(this).get(DetailViewModel::class.java)
		binding     = ActivityDetailBinding.inflate(layoutInflater)
		setContentView(binding.root)

		//inisialisasi Database
//		val storage = RestoranDatabase.getInstance(this).restoranDao

		// Mengambil data dari halaman sebelumnya
		val data        = intent.getParcelableExtra<ListData>("DATA")

		// Memasukkan data pada viewModel
		if (data != null) {
			vmDetail.setData(data)
		}

		// Observer untuk LiveData
		vmDetail.data.observe(this, Observer { data ->
			Glide.with(this@DetailActivity)
				.load(data.imageUrl)
				.into(binding.imgDetail)
			binding.nameDetail.text     = data.nama
			binding.priceDetail.text    = data.hargaFormat.toString()
			binding.descDetail.text     = data.detail
			binding.locationDetail.text = data.alamatResto
		})

		binding.btnDetail.setOnClickListener{
			// Masukkan value ke dalam database
			data?.let {
				val restoranData = Restoran(
					name = it.nama.toString(),
					price = it.harga, // Ubah tipe data sesuai dengan definisi di data class
					quantity = 1, // Ganti dengan kuantitas yang sesuai
					note = "",
					img = it.imageUrl.toString()
				)

				val check = vmDetail.findItem(this,restoranData)

				if (!check){
					// insert Data
					vmDetail.insertItem(this@DetailActivity,restoranData)
				} else {
					// Update quantity jika orderannya sama
					vmDetail.updateItem(this@DetailActivity,restoranData)
				}
				Toast.makeText(this, "Pesanan sudah di keranjang!", Toast.LENGTH_SHORT).show()
			}

			// Lanjut ke Halaman Berikutnya
			val intent = Intent(this,MainActivity::class.java)
			startActivity(intent)
		}
	}
}