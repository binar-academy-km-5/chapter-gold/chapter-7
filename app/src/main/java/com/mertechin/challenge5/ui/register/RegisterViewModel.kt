package com.mertechin.challenge5.ui.register

import android.content.Context
import android.content.SharedPreferences
import android.util.Log
import androidx.lifecycle.ViewModel
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.ktx.auth
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase
import com.mertechin.challenge5.dataclass.room_dataclass.User

class RegisterViewModel : ViewModel() {
	private lateinit var auth: FirebaseAuth
	private lateinit var sharedPreferences: SharedPreferences

	fun initViewModel(context: Context){
		sharedPreferences = context.getSharedPreferences("login_pref", Context.MODE_PRIVATE)
	}

	fun registerFirebase(email: String, password: String, callback: RegisterCallback) {
		auth = Firebase.auth

		auth.createUserWithEmailAndPassword(email, password)
			.addOnCompleteListener {
				if (it.isSuccessful) {
					// Daftarkan sharedPref
					setSharedPref(email)

					// Memberi callback Success
					callback.onRegisterSuccess()
				} else {
					// User belum terdaftar, Maka berikan callback Gagal
					callback.onRegisterFailure()
				}
			}
	}

	fun registerDatabase(username: String, password: String, email: String, noTelp: String){
		// Mengambil UID User
		val currentUser = FirebaseAuth.getInstance().currentUser?.uid
		val userData    = User(username,password,email,noTelp)

		Log.e("UID", currentUser.toString())
		addNewUser(currentUser,userData)
	}

	private fun addNewUser(id: String?, user: User) {
		val db = Firebase.firestore

		// Menambahkan dokumen baru ke koleksi "users"
		if (id != null) {
			db.collection("users").document(id).set(user).addOnSuccessListener {
					Log.d("Firestore Status","Berhasil Menambahkan Data!")
				}.addOnFailureListener { e ->
					Log.e("Firestore Status","Gagal menambahkan data. Pesan Error: $e")
				}
		}
	}

	fun setSharedPref(email: String){
		val editor = sharedPreferences.edit()
		editor.putString("USER_EMAIL",email)
		editor.apply()
	}

	interface RegisterCallback {
		fun onRegisterSuccess()
		fun onRegisterFailure()
	}
}