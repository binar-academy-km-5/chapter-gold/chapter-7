package com.mertechin.challenge5.ui.cart

import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.mertechin.challenge5.dataclass.room_dataclass.Restoran
import com.mertechin.challenge5.repository.RestaurantRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class CartViewModel(private val repo: RestaurantRepository): ViewModel() {
	fun getCart(context: Context): LiveData<List<Restoran>>{
		return repo.getAllCart()
	}

	fun getSUM(context: Context): LiveData<Int> {
		return repo.measure()
	}

	fun updateNote(context: Context, restoran: Restoran, note: String){
		val data    = repo.findSomeDataByName(restoran.name)

		viewModelScope.launch(Dispatchers.IO){
			repo.updateNote(data.id,note)
		}
	}

	fun deleteItem(context: Context, restoran: Restoran){
		val data    = repo.findSomeDataByName(restoran.name)

		viewModelScope.launch(Dispatchers.IO){
			repo.deleteWhere(data.id)
		}
	}

	fun increaseAmount(context: Context, restoran: Restoran) {
		val data    = repo.findSomeDataByName(restoran.name)

		viewModelScope.launch(Dispatchers.IO){
			repo.updateQuantity(data.id,data.quantity+1)
		}
	}

	fun decreaseAmount(context: Context, restoran: Restoran){
		val data    = repo.findSomeDataByName(restoran.name)
		var qty     = data.quantity

		viewModelScope.launch(Dispatchers.IO){
			if (qty <= 1){
				repo.deleteWhere(data.id)
			} else {
				repo.updateQuantity(data.id,qty-1)
			}
		}
	}
}