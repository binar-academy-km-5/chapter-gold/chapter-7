package com.mertechin.challenge5.ui.checkout

import android.content.Context
import android.content.SharedPreferences
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import com.mertechin.challenge5.dataclass.room_dataclass.Restoran
import com.mertechin.challenge5.repository.RestaurantRepository

class CheckoutViewModel(private val repo: RestaurantRepository) : ViewModel() {
	private lateinit var sharedPreferences: SharedPreferences
	var totalHarga: Int = 0

	fun initSharedPref(context: Context){
		sharedPreferences = context.getSharedPreferences("login_pref", Context.MODE_PRIVATE)
	}

	fun getSharedPref() : String?{
		return sharedPreferences.getString("USER_EMAIL", "")
	}

	fun getCart(context: Context): LiveData<List<Restoran>> {
		return repo.getAllCart()
	}

	fun getSUM(context: Context): LiveData<Int> {
		return repo.measure()
	}

	fun getTotalHarga(ctx: Context, owner: LifecycleOwner){
		val hitung = repo.measure()
		hitung.observe(owner) {
			if (it != null) totalHarga = it.toInt() else totalHarga = 0
		}
	}

	fun deleteData() {
		repo.deleteAll()
	}


}