package com.mertechin.challenge5.ui.login

import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.mertechin.challenge5.MainActivity
import com.mertechin.challenge5.databinding.ActivityLoginBinding
import com.mertechin.challenge5.ui.register.RegisterActivity
import org.koin.androidx.viewmodel.ext.android.viewModel

class LoginActivity : AppCompatActivity() {
	private lateinit var binding : ActivityLoginBinding
//	private lateinit var loginViewModel: LoginViewModel
	private val loginViewModel: LoginViewModel by viewModel()

	override fun onCreate(savedInstanceState: Bundle?) {
		super.onCreate(savedInstanceState)
		binding = ActivityLoginBinding.inflate(layoutInflater)
		setContentView(binding.root)

//		loginViewModel = ViewModelProvider(this).get(LoginViewModel::class.java)

		//Inisialisasi sharedPref
		loginViewModel.initSharedPref(this)

		// Ambil nilai dari form Login
		val email = binding.etEmail.text
		val pass  = binding.etPassword.text

		binding.btnLogin.setOnClickListener{
			// Verifikasi ke Firebase

			loginViewModel.loginFirebase(email.toString(),pass.toString(), object : LoginViewModel.LoginCallback{
				override fun onLoginSuccess() {
					// pindah ke activity selanjutnya
					val intent = Intent(this@LoginActivity,MainActivity::class.java)
					startActivity(intent)
				}

				override fun onLoginFailure() {
					val toast = Toast.makeText(this@LoginActivity,"Email atau Password Salah!", Toast.LENGTH_SHORT)
					toast.show()
				}
			})
		}

		binding.tvRegister.setOnClickListener {
			val intent = Intent(this,RegisterActivity::class.java)
			startActivity(intent)
		}
	}
}