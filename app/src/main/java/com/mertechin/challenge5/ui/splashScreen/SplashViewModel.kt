package com.mertechin.challenge5.ui.splashScreen

import android.content.Context
import android.content.SharedPreferences
import androidx.lifecycle.ViewModel

class SplashViewModel : ViewModel() {
	private lateinit var sharedPreferences: SharedPreferences

	fun initSharedPref(context: Context){
		sharedPreferences = context.getSharedPreferences("login_pref", Context.MODE_PRIVATE)
	}

	fun getSharedPref() : String?{
		return sharedPreferences.getString("USER_EMAIL", "")
	}

	fun checkPref(): Boolean {
		val userEmail = sharedPreferences.getString("USER_EMAIL", "").toString()

		return userEmail.isNullOrEmpty()
	}
}