package com.mertechin.challenge5.ui.cart

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.mertechin.challenge5.adapter.CartAdapter
import com.mertechin.challenge5.databinding.FragmentCartBinding
import com.mertechin.challenge5.ui.checkout.CheckoutActivity
import org.koin.androidx.viewmodel.ext.android.viewModel

class CartFragment : Fragment() {

	private var _binding: FragmentCartBinding? = null
	private val vmCart: CartViewModel by viewModel()
	private lateinit var cartAdapter: CartAdapter

	// This property is only valid between onCreateView and
	// onDestroyView.
	private val binding get() = _binding!!

	override fun onCreateView(
		inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
	): View {
		_binding = FragmentCartBinding.inflate(inflater, container, false)

		val rvCart = binding.rvCart
		rvCart.layoutManager = LinearLayoutManager(requireContext())

//		val vmCart = ViewModelProvider(this).get(CartViewModel::class.java)
		vmCart.getCart(requireContext()).observe(viewLifecycleOwner) { data ->
			// Inisialisasi adapter dan set adapter setelah data tersedia
			cartAdapter = CartAdapter(data,vmCart)
			rvCart.adapter = cartAdapter
		}

		vmCart.getSUM(requireContext()).observe(viewLifecycleOwner){
			var totalHarga = it
			if (totalHarga == null) totalHarga = 0

			binding.tvHargaTotal.text = totalHarga.toString()
		}

		binding.btnCheckout.setOnClickListener{
			// Ambil data untuk ditampilkan ke GMaps
			val intent = Intent(requireContext(),CheckoutActivity::class.java)
			startActivity(intent)
		}

		val root: View = binding.root
		return root
	}

	override fun onDestroyView() {
		super.onDestroyView()
		_binding = null
	}
}