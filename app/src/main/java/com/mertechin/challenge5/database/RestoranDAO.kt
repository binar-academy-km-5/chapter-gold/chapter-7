package com.mertechin.challenge5.database

import androidx.lifecycle.LiveData
import androidx.room.*
import com.mertechin.challenge5.dataclass.data_update
import com.mertechin.challenge5.dataclass.room_dataclass.Restoran

@Dao
interface RestoranDAO {
	@Insert
	fun insert(restoran: Restoran)

	@Query("SELECT SUM(price * quantity) FROM tbl_keranjang")
	fun measure(): LiveData<Int>

	@Query("SELECT * FROM tbl_keranjang")
	fun getAllCart(): LiveData<List<Restoran>>

	@Query("SELECT * FROM tbl_keranjang WHERE name=:nama")
	fun findItemCart(nama: String): List<Restoran>

	@Query("SELECT id,quantity FROM tbl_keranjang WHERE name=:nama")
	fun findSomeDataByName(nama: String): data_update

	@Update
	fun update(restoran: Restoran)

	@Query("UPDATE tbl_keranjang SET quantity = :newQuantity WHERE id = :restoranId")
	fun updateQuantity(restoranId: Long, newQuantity: Int)

	@Query("UPDATE tbl_keranjang SET note =:note WHERE id = :cartId")
	fun updateNote(cartId: Long, note: String)

	@Query("DELETE FROM tbl_keranjang")
	fun deleteAll()

	@Query("DELETE FROM tbl_keranjang WHERE id=:id")
	fun deleteWhere(id : Long)
}