package com.mertechin.challenge5.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.mertechin.challenge5.R
import com.mertechin.challenge5.dataclass.room_dataclass.Restoran

class CheckoutAdapter(private val data: List<Restoran>) : RecyclerView.Adapter<CheckoutHolder>() {
	override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CheckoutHolder {
		val inflater = LayoutInflater.from(parent.context)
		val view = inflater.inflate(R.layout.checkout_menu_item, parent, false)
		return CheckoutHolder(view)
	}

	override fun onBindViewHolder(holder: CheckoutHolder, position: Int) {
		val menuHolder = holder
		menuHolder.onBind(data[position])
	}

	override fun getItemCount(): Int {
		return data.size
	}
}

class CheckoutHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
	val ivGambar : ImageView    = itemView.findViewById(R.id.ivCheckoutProfile)
	val tvJudul  : TextView     = itemView.findViewById(R.id.tvCheckoutJudul)
	val tvharga  : TextView     = itemView.findViewById(R.id.tvCheckoutHarga)
	val tvQty    : TextView     = itemView.findViewById(R.id.tvCheckoutQuantity)
	val tvNote   : TextView     = itemView.findViewById(R.id.tvCheckNote)

	fun onBind(dataCart: Restoran){
		Glide.with(itemView.context)
			.load(dataCart.img)
			.into(ivGambar)

		tvJudul.text    = dataCart.name
		tvharga.text    = dataCart.price.toString()
		tvQty.text      = dataCart.quantity.toString()
		tvNote.text     = dataCart.note
	}
}