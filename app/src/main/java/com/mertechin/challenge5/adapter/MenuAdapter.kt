package com.mertechin.challenge5.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.mertechin.challenge5.R
import com.mertechin.challenge5.dataclass.api_dataclass.ListData

class MenuAdapter(private val isGrid: Boolean, private val data: List<ListData>) : RecyclerView.Adapter<RecyclerView.ViewHolder>()  {

	private var onItemClickCallback: OnItemClickCallback? = null

	fun setOnItemClickCallback(callback: OnItemClickCallback) {
		onItemClickCallback = callback
	}

	override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
		val inflater = LayoutInflater.from(parent.context)
		val layoutRes = if (isGrid) R.layout.grid_item_menu else R.layout.linear_item_menu
		val view = inflater.inflate(layoutRes, parent, false)

		return if (isGrid) GridMenuHolder(view) else LinearMenuHolder(view)
	}

	override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
		val menuHolder = holder as MenuHolder
		menuHolder.onBind(data[position])

		menuHolder.itemView.setOnClickListener {
			onItemClickCallback?.onItemClicked(data[position])
		}
	}

	override fun getItemCount(): Int = data.size

	interface OnItemClickCallback {
		fun onItemClicked(data: ListData)
	}

	abstract class MenuHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
		abstract fun onBind(menu: ListData)
	}

	inner class GridMenuHolder(itemView: View) : MenuHolder(itemView) {
		private val gambar: ImageView = itemView.findViewById(R.id.image_menu_grid)
		private val tvJudul: TextView = itemView.findViewById(R.id.title_menu_grid)
		private val tvHarga: TextView = itemView.findViewById(R.id.price_menu_grid)

		override fun onBind(menu: ListData) {
			tvJudul.text = menu.nama
			tvHarga.text = menu.hargaFormat
			Glide.with(itemView.context)
				.load(menu.imageUrl)
				.into(gambar)
		}
	}

	inner class LinearMenuHolder(itemView: View) : MenuHolder(itemView) {
		private val gambar: ImageView = itemView.findViewById(R.id.image_menu_linear)
		private val tvJudul: TextView = itemView.findViewById(R.id.title_menu_linear)
		private val tvHarga: TextView = itemView.findViewById(R.id.price_menu_linear)

		override fun onBind(menu: ListData) {
			tvJudul.text = menu.nama
			tvHarga.text = menu.hargaFormat
			Glide.with(itemView.context)
				.load(menu.imageUrl)
				.into(gambar)
		}
	}
}


