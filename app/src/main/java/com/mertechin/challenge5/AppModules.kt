package com.mertechin.challenge5

import com.mertechin.challenge5.database.RestoranDatabase
import com.mertechin.challenge5.repository.RestaurantRepository
import com.mertechin.challenge5.ui.cart.CartViewModel
import com.mertechin.challenge5.ui.checkout.CheckoutViewModel
import com.mertechin.challenge5.ui.dashboard.DashboardViewModel
import com.mertechin.challenge5.ui.detail.DetailViewModel
import com.mertechin.challenge5.ui.login.LoginViewModel
import com.mertechin.challenge5.ui.profile.ProfileViewModel
import com.mertechin.challenge5.ui.register.RegisterViewModel
import com.mertechin.challenge5.ui.splashScreen.SplashViewModel
import org.koin.android.ext.koin.androidContext
import org.koin.androidx.viewmodel.dsl.viewModelOf
import org.koin.core.module.Module
import org.koin.dsl.module

object AppModules {
	private val databaseModule = module {
		single { RestoranDatabase.getInstance(androidContext()) }
		single { get<RestoranDatabase>().restoranDao() }
	}

	private val repositoryModule = module {
		single { RestaurantRepository(get()) }
	}

	private val viewModelModule = module {
		viewModelOf(::SplashViewModel)
		viewModelOf(::LoginViewModel)
		viewModelOf(::RegisterViewModel)
		viewModelOf(::CartViewModel)
		viewModelOf(::CheckoutViewModel)
		viewModelOf(::DashboardViewModel)
		viewModelOf(::DetailViewModel)
		viewModelOf(::ProfileViewModel)
	}

	val modules: List<Module> = listOf(
		databaseModule,
		repositoryModule,
		viewModelModule
	)
}