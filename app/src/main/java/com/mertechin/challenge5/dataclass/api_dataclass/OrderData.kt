package com.mertechin.challenge5.dataclass.api_dataclass

data class OrderData(
	var nama    : String,
	var qty     : Byte,
	var catatan : String,
	var harga   : Int
)
