package com.mertechin.challenge5.dataclass.api_dataclass

import android.os.Parcel
import android.os.Parcelable

data class ListData(
    val alamatResto: String?,
    val createdAt: String?,
    val detail: String?,
    val harga: Int,
    val hargaFormat: String?,
    val id: Int,
    val imageUrl: String?,
    val nama: String?,
    val updatedAt: String?
) : Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readInt(),
        parcel.readString(),
        parcel.readInt(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString()
    ) {
    }

    override fun describeContents(): Int {
        return 0
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(alamatResto)
        parcel.writeString(createdAt)
        parcel.writeString(detail)
        parcel.writeInt(harga)
        parcel.writeString(hargaFormat)
        parcel.writeInt(id)
        parcel.writeString(imageUrl)
        parcel.writeString(nama)
        parcel.writeString(updatedAt)
        
    }

    companion object CREATOR : Parcelable.Creator<ListData> {
        override fun createFromParcel(parcel: Parcel): ListData {
            return ListData(parcel)
        }

        override fun newArray(size: Int): Array<ListData?> {
            return arrayOfNulls(size)
        }
    }
}