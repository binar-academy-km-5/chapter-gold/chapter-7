package com.mertechin.challenge5.dataclass.room_dataclass

data class User(
	val username : String,
	val password : String,
	val email : String,
	val notelp : String
)
