package com.mertechin.challenge5.dataclass

data class data_update(
	val id: Long, val quantity: Int
)
