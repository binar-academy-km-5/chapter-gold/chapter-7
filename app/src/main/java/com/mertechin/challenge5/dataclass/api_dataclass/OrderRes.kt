package com.mertechin.challenge5.dataclass.api_dataclass

data class OrderRes(
	val status : String,
	val message: String,
	val code   : Short
)
