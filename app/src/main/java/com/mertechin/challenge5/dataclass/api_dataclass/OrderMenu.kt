package com.mertechin.challenge5.dataclass.api_dataclass

data class OrderMenu(
	var username        : String,
	var total           : Int,
	var orders          : List<OrderData>
)
