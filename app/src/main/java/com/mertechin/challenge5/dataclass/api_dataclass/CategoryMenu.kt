package com.mertechin.challenge5.dataclass.api_dataclass

data class CategoryMenu(
	val `data`: List<CategoryData>,
	val message: String,
	val status: Boolean
)
