package com.mertechin.challenge5.dataclass.api_dataclass

data class CategoryData(
    val createdAt: String,
    val id: Int,
    val imageUrl: String,
    val nama: String,
    val updatedAt: String
)