package com.mertechin.challenge5

import com.mertechin.challenge5.api.ApiClient
import com.mertechin.challenge5.api.ApiServices
import com.mertechin.challenge5.dataclass.api_dataclass.CategoryMenu
import com.mertechin.challenge5.dataclass.api_dataclass.ListMenu
import com.mertechin.challenge5.dataclass.api_dataclass.OrderData
import com.mertechin.challenge5.dataclass.api_dataclass.OrderMenu
import com.mertechin.challenge5.dataclass.api_dataclass.OrderRes
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import retrofit2.Call
import retrofit2.Response

class UnitTesting {
	private lateinit var apiService: ApiServices

	@Before
	fun setUp() {
		apiService = ApiClient.ApiClient.instance
	}

	@Test
	fun testBaseUrl() {
		// Mengecek apakah BASE_URL adalah Sama yang digunakan dalam APIClient
		val urlString = "https://testing.jasa-nikah-siri-amanah-profesional.com/"
		assertEquals(urlString, ApiClient.ApiClient.BASE_URL)
	}

	@Test
	fun testGetListMenuApi() {
		// Mengecek apakah GetListMenu dapat di GET oleh API?
		val call: Call<ListMenu> = apiService.getListMenu()
		val response: Response<ListMenu> = call.execute()

		assertEquals(true, response.isSuccessful)
	}

	@Test
	fun testGetCategoryApi() {
		// Mengecek apakah CategoryMenu dapat di GET oleh API?
		val call: Call<CategoryMenu> = apiService.getCategory()
		val response: Response<CategoryMenu> = call.execute()

		assertEquals(true, response.isSuccessful)
	}

	@Test
	fun testSendOrderApi() {
		// Mengecek apakah dapat mengirimkan POST melalui API?
		val orderMenu = OrderMenu(
			username = "Harisrizz",
			total = 90000,
			orders = listOf(
				OrderData("Nasi Goreng", 3, "Sedikit Pedas GPP", 30000),
			)
		)

		// Kirim pesanan menggunakan metode sendOrder
		val call: Call<OrderRes> = apiService.sendOrder(orderMenu)
		val response: Response<OrderRes> = call.execute()

		assertEquals(true, response.isSuccessful)
	}

}